# network security report #

## Introduction ##

This report is about the usage of snort IDS (Intrusion & Detection System). For this, I have set up 2 virtual machines on the same NAT network. One will be the possibly vulnerable server (ubuntu) and the other one (kali linux) the attacker. I will show how snort detect possible threats.

## Sniffer mode ##

![ping from kali linux](img/kali-ping.png)

![ping on ubuntu](img/ubuntu-ping.png)

## Packet logger mode ##

It's also possible to record the packets on the disk in order to analyse it later with the command `./snort -dev -l ./log-directory`

![snort logger mode](img/snort-logger-mode.png)

## Network Intrusion Detection System mode ##

To enable Network Intrusion Detection System (NIDS) mode so that you don’t record every single packet sent down the wire, it's possible to link it to a configuration file which is state some rules. By default, the configuration file is in `/etc/snort/snort.conf`

## snort configuration ##

By default, we can find `/etc/snort/snort.conf` which include different rules files in `/etc/snort/rules`. The rules are mentioned as below :
`<Rule Actions> <Protocol> <Source IP Address> <Source Port> <Direction Operator> <Destination IP Address> <Destination > (rule options)`

In `/etc/snort/snort.conf` :
It's recommended to set `ipvar HOME_NET the-ip-address-you-want-to-protect` and `ipvar EXTERNAL_NET !$HOME_NET` to avoid detecting attack from our own internal network

![snort conf HOME_NET](img/snort-conf-home_net.png)

### Rule Header ###

An infography from the official snort website is available to understand how to write rules at this address : https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/000/116/original/Snort_rule_infographic.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIXACIED2SPMSC7GA%2F20180724%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20180724T123821Z&X-Amz-Expires=172800&X-Amz-SignedHeaders=host&X-Amz-Signature=d2a3f4308b4134082210bcca92cfc4ccb600605eb2aae488f5468659050ec361

![snort rules](img/rules-infography.png)

Explanation of `alert tcp $EXTERNAL_NET any -> $HOME_NET 21 (msg: "incoming ftp connection lol"; flags:S; sid:10000;)`
- alert – Rule action. Snort will generate an alert when the set condition is met.
- tcp – protocol (choice between tcp, udp, icmp etc...)
- $EXTERNAL_NET – source
- any – Source IP. Snort will look at all sources.
- -> – Direction. From source to destination.
- $HOME_NET – Destination IP. We are using the HOME_NET value from the snort.conf file.
- 21 – Source port. the ftp on that case

### Let's set our rules ###

By default, `/etc/snort/snort.conf` includes a set of `.rules` files where specific rules are set. Some includes are commented but can be uncommented for more specific purposes.

![snort conf rules include](img/snort-conf-rules-include.png)

In order to understand how the rules are used, I'll comment any rules include and just create my own rules on `/etc/snort/rules/myrules.rules`

I want to try an easy first rule in order to be sure that my rules are implemented, I'll use this command line to try snort : `sudo snort -d -l /var/log/snort -c /etc/snort/snort.conf -A console` with the following rule : `alert tcp $EXTERNAL_NET any -> $HOME_NET 21 (msg: "incoming ftp connection lol"; flags:S; sid:10001;)`

![ftp connection detection rule](img/ftp-connection.png)

with `alert tcp $EXTERNAL_NET any -> $HOME_NET 80 (msg: "incoming http connection lol"; sid:10002;)`

![http connection detection rule](img/http-incoming.png)

with `alert icmp $EXTERNAL_NET any -> $HOME_NET any (msg: "someone is pinging"; sid:10003;)`

![ping detection](img/ping.png)

It's possible to read the log files of snort simply running `snort -r snort.log`

![snort -r ](img/snort-r.png)

### Identify a nmap scan ###

## nmap ping scan ##

An attacker would like to identify if the computer is alive on the network with a ping scan with `nmap -sP 10.0.2.15 --disable-arp-ping`

`alert icmp $EXTERNAL_NET any -> $HOME_NET any (msg: "NMAP ping sweep Scan"; dsize:0;sid:10004;)`

![nmap ping](img/nmap-ping.png)

## nmap TCP scan ##
Attacker can make a networking enumeration on TCP with `nmap -sT 10.0.2.15`

`alert tcp $EXTERNAL_NET any -> $HOME_NET any (msg: "NMAP TCP Scan";sid:10005;)`

![nmap ping](img/nmap-tcp.png)

## nmap XMAS scan ##

An attacker can send data with Fin, PSH, and URG flags with `nmap -sX 10.0.2.15`

`alert tcp $EXTERNAL_NET any -> $HOME_NET any (msg:"NMAP XMAS Tree Scan"; flags:FPU; sid:10006;)`

![nmap xmas](img/nmap-xmas.png)

Thank you for reading
